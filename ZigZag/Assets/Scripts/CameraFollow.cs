﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject ball;
    Vector3 offset;
    public float lerpRate;
    public bool gameOver;

	// Use this for initialization
	void Start () {
        // ball postion - camera position
        offset = ball.transform.position - transform.position;
        gameOver = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!gameOver)
        {
            Follow();
        }
	}

    void Follow()
    {
        // camera current position
        Vector3 pos = transform.position;

        // ball current position to the target position
        Vector3 targetPos = ball.transform.position - offset;

        // calculate the camera from the current postion to the target pos at this smooth rate
        pos = Vector3.Lerp(pos, targetPos, lerpRate * Time.deltaTime);

        // move the camera to the new position
        transform.position = pos;
    }
}
