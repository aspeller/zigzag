﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour {

    public GameObject platform;
    public GameObject diamonds;

    // last position of the platform
    Vector3 lastPos;

    //
    float size;

    public bool gameOver;

	// Use this for initialization
	void Start () {
        lastPos = platform.transform.position;
        size = platform.transform.localScale.x;

        for (int i = 0; i < 20; i++)
        {
            SpawnPlatforms();
        }

    }

    public void StartSpawningPlatforms()
    {
        InvokeRepeating("SpawnPlatforms", 0.1f, 0.2f);
    }
	
	// Update is called once per frame
	void Update () {
        if (GameManager.instance.gameOver)
        {
            CancelInvoke("SpawnPlatforms");
        }
	}

    void SpawnPlatforms()
    {
        if (gameOver)
        {
            return;
        }

        SpawnXorY();
    }

    void SpawnXorY()
    {
        Vector3 pos = lastPos;

        int rand = Random.Range(0, 101);
        if (rand > 51)
        {
            pos.x += size;
        } else
        {
            pos.z += size;
        }
        
        lastPos = pos;
        Instantiate(platform, pos, Quaternion.identity);
        SpawnDiamond(pos);
        
        
    }

    void SpawnDiamond(Vector3 pos)
    {
        int rand = Random.Range(0, 4);

        if (rand < 1)
        {
            Instantiate(diamonds, new Vector3(pos.x,pos.y+1,pos.z), diamonds.transform.rotation);
        }
    }
}
